export default (await import('vue')).defineComponent({
props: ['comment'],
data() {
return {
value: 'Marfino Hamzah'
};
},

filters: {
presenceFormat(presence) {
if (presence == 1)
return 'Berkenan hadir';

else
return 'Maaf tidak bisa hadir';
},
guestFirstName(name) {
return name.charAt(0).toUpperCase();
}
},

methods: {
badgeClass(presence) {
if (presence == 1)
return 'tw-bg-opacity-50 tw-bg-brown-lighter tw-text-brown-dark';

else
return 'tw-bg-opacity-10 tw-bg-gray-500 tw-text-gray-500';
}
},
});

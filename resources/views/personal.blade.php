<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="title" content="Faradina & Fino">
    <meta name="description" content="Landing page wedding Faradina & Fino">
    <meta name="keywords" content="Faradina & Fino, wedding, wedding invitation, Waroeng Ji Nung">
    <meta name="author" content="Faradina & Fino, wedding, wedding invitation, Waroeng Ji Nung">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5">
    <meta name="format-detection" content="telephone=no">
    <meta property="og:image" content="{{ asset('assets/images/gallery/7n.jpg') }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:image" content="{{ asset('assets/images/gallery/7n.jpg') }}">
    <title>Marfino Hamzah ♡ Faradina Dian Rumada</title>

    <!-- Fonts -->
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/flickity.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/uikit.css') }}">
    <link rel="stylesheet" href="{{ asset('css/invitation-dark.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sakura.min.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/logo.ico') }}"/>
    <!-- CSS -->
    {{-- <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css"> --}}

    <style>

    @font-face {
        font-family: 'Brittany Signature';
        src: url('assets/fonts/BrittanySignature.ttf') format('truetype');
        font-weight: normal;
        font-style: normal;
    }

    .custom-font {
        font-family: 'Brittany Signature', sans-serif;font-weight: bold;
    }
        h1,
        h2,
        h3,
        h4,
        h5 {
            font-family: Wenceslas-Oblique;
            color: #f4f4f4 !important;
        }
    </style>
    <style>
        body{background-color:#aea38e;color:#dddbd7;}.yn-color{font-family:Scarlet;background:#dddbd7;-webkit-background-clip:text;-webkit-text-fill-color:transparent}
        @media (max-width: 720px) {
            .responsive-img {
                width: 50%;
                height: auto;
            }
        }

@media (min-width: 1024px) { /* Ukuran layar untuk laptop dan lebih besar */
    .responsive-img {
        width: 25%;
    }
}
    </style>
</head>
<body uk-scrollspy="target: .yn-anim; cls: uk-animation-fade; delay: 350">
{{--
    warna hijau terkiri #53583e, warna abu" terkiri kedua #aea38e, warna putih terkanan kedua #dddbd7, warna coklat terkanan #593b1f --}}
    <div id="root">
        <router-view></router-view>

        <div class="tw-h-64"></div>
        <audio id="audio" autoplay loop>
            <source src="assets/music/music.mp3" type="audio/mp3">
            Your browser does not support the audio element.
        </audio>
        <div id="my_id" uk-modal="" bg-close="false" class="uk-modal uk-flex uk-open" tabindex="0">
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-text-center" style="background: transparent; background-image: url({{ url('assets/images/evp.webp') }}); padding:20px 15px 60px 15px; background-position: center;
            background-repeat: unset;
            background-size: cover; border-radius:16px;  height: 400px;
            width: 342px;
            ">
                <div class="@if(request()->has('name')) tw-mt-20 @else tw-mt-24 @endif tw-text-center">
                    <p class="uk-text-medium uk-margin-remove-bottom txt-simple">
                        Kepada Yth<br>
                        Bp/ibu/sudara/i</p>

                    @if (request()->has('name'))
                        <div class="tw-mb-5 tw-mt-0.5 tw-text-base tw-w-semibold tw-text-center">
                            {{ request()->get('name') }}
                        </div>
                    @else
                        <div class="tw-mb-3"></div>
                    @endif

                    <p class="uk-text-medium txt-simple">YOU'RE INVITED TO OUR WEDDING</p>
                    <h2 class="yn-color tw-mt-4 custom-font" style="line-height:1; margin-bottom:9px !important; font-size:24px;">
                        Faradina &amp; Fino	</h2>
                </div>

                <div class="tw-mt-20">
                    <button id="play-sound" class="uk-modal-close tw-bg-white tw-text-gray-600 tw-text-xs tw-tracking-widest tw-py-2 tw-px-4 tw-rounded-lg tw-w-3/5" type="button">OPEN INVITATION</button>
                </div>

            </div>
        </div>
    </div>
</body>
<footer>
<div>
    <img src="assets/images/frame-bl.webp" alt="Frame BL" class="responsive-img animate__animated animate__fadeInUp animate__slow bg-auto md:bg-contain" style="bottom: 0; left: 0px; position: fixed; z-index: 1;">
    <img src="assets/images/frame-bl-2.webp" alt="Frame BL 2" class="responsive-img animate__animated animate__fadeInBottomLeft animate__slow bg-auto md:bg-contain" style="bottom: 0; left: 0px; position: fixed; z-index: 2;">
    <img src="assets/images/frame-br.webp" alt="Frame BL" class="responsive-img animate__animated animate__fadeInBottomRight animate__slower bg-auto md:bg-contain" style="bottom: 0; right: 0px; position: fixed; z-index: 1;">
    <img src="assets/images/frame-br-2.webp" alt="Frame BL 2" class="responsive-img animate__animated animate__fadeInBottomRight animate__slower bg-auto md:bg-contain" style="bottom: 0; right: 0px; position: fixed; z-index: 2;">
</div>
<img src="assets/images/frame-bm.webp" alt="Frame BM"  class="tw-mx-auto tw-mb-3 tw-sm:mb-5 animate__animated animate__fadeInUp animate__slow">
</footer>

<!-- JavaScript -->
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="{{ asset('assets/js/uikit.min.js') }}"></script>
<script src="{{ asset('assets/js/sakura.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    var sakura = new Sakura('body', {
    maxSize : 14,
    delay: 100,
    fallspeed: 3,
    colors: [
        {
            gradientColorStart: 'rgba(255, 183, 197, 0.9)',
            gradientColorEnd: 'rgba(255, 197, 208, 0.9)',
            gradientColorDegree: 120,
        },
        {
            gradientColorStart: 'rgba(255,189,189)',
            gradientColorEnd: 'rgba(227,170,181)',
            gradientColorDegree: 120,
        },
        {
            gradientColorStart: 'rgba(212,152,163)',
            gradientColorEnd: 'rgba(242,185,196)',
            gradientColorDegree: 120,
        },
    ],
});

</script>
</html>

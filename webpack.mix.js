const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        extensions: [".wasm", ".mjs", ".js", ".jsx", ".json", ".vue"],
        // Mungkin ada konfigurasi lainnya di sini...
    },
});

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .postCss('resources/css/app.css', 'public/css', [
        require('tailwindcss'),
    ]);

mix.browserSync('127.0.0.1:8000');
